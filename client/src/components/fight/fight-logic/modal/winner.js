import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const title = 'Game Over';
  const bodyElement = `${fighter.name} is win! Close the window to restart.`;
  showModal({
    title,
    bodyElement,
    onClose() {
      window.location.reload();
    },
  });
}
