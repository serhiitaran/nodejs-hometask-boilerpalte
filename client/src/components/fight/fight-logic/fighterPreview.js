import { createElement } from './helpers/domHelper';
import { IMAGES } from './constants/fighterSource';

export function createFighterImage(fighter) {
  const { name } = fighter;
  const getRandomImg = (images) =>
    images[Math.floor(Math.random() * images.length)];
  const randomImg = getRandomImg(IMAGES);
  const attributes = {
    src: randomImg,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
