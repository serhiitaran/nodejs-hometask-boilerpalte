const { checkName } = require('./baseValidator');

const checkPhoneNumber = (number) => {
  const validLength = 13;
  const regex = /^\+?3?8?(0\d{9})$/;
  const isValidLength = number.trim().length === validLength;
  return regex.test(number) && isValidLength;
};

const checkEmail = (email) => {
  const regex = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@(?:gmail|GMAIL)([\.])(?:com|COM)/;
  return regex.test(email);
};

const checkPassword = (password) => {
  return password.length >= 3;
};

const userValidator = {
  firstName: checkName,
  lastName: checkName,
  email: checkEmail,
  phoneNumber: checkPhoneNumber,
  password: checkPassword,
};

exports.userValidator = userValidator;
