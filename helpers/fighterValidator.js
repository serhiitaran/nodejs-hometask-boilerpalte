const { checkName } = require('./baseValidator');

const checkValue = (value, min, max) => {
  const isNumber = Number.isInteger(+value);
  const inRange = value >= min && value <= max ? true : false;
  return isNumber && inRange;
};

const checkHealth = (health) => {
  const min = 1;
  const max = 100;
  return checkValue(health, min, max);
};

const checkPower = (power) => {
  const min = 1;
  const max = 99;
  return checkValue(power, min, max);
};

const checkDefense = (defense) => {
  const min = 1;
  const max = 9;
  return checkValue(defense, min, max);
};

const fighterValidator = {
  name: checkName,
  health: checkHealth,
  power: checkPower,
  defense: checkDefense,
};

exports.fighterValidator = fighterValidator;
