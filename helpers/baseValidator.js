exports.checkName = (name) => {
  const minLength = 2;
  const maxLength = 50;

  const checkLenght = (str, minLength, maxLength) =>
    str.trim().length >= minLength && str.trim().length <= maxLength;
  const isValidLength = checkLenght(name, minLength, maxLength);
  return isValidLength;
};
