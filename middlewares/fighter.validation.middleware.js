const { fighter } = require('../models/fighter');
const { fighterValidator } = require('../helpers/fighterValidator');
const {
  addDefaultHealth,
  deleteExtraFields,
  checkRequiredFields,
  validateFields,
  checkErrors,
} = require('./helpers');

const createFighterValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    addDefaultHealth(req.body, fighter);
    deleteExtraFields(fields, fighter, errors);
    checkRequiredFields(fields, fighter, errors);
    validateFields(fields, fighterValidator, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    addDefaultHealth(fields, fighter);
    deleteExtraFields(fields, fighter, errors);
    validateFields(fields, fighterValidator, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
