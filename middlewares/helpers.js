const { BadRequest } = require('../helpers/customErrors');

exports.addDefaultHealth = (fields, { health }) => {
  const defaultHealth = { health };
  Object.assign(fields, defaultHealth);
};

exports.deleteExtraFields = (fields, model, errors) => {
  for (const field in fields) {
    if (!model.hasOwnProperty(field) || field === 'id') {
      delete fields[field];
      errors.push(`The field '${field}' should be absent.`);
    }
  }
};

exports.checkRequiredFields = (fields, model, errors) => {
  for (const field in model) {
    if (!fields.hasOwnProperty(field) && field !== 'id') {
      errors.push(`The field '${field}' is required.`);
    }
  }
};

exports.validateFields = (fields, validator, errors) => {
  for (const field in fields) {
    const fieldValue = fields[field];
    const fieldValidator = validator[field];
    const isValidValue = fieldValidator(fieldValue);
    if (!isValidValue) {
      errors.push(`The value of field '${field}' is invalid.`);
    }
  }
};

exports.checkErrors = (errors) => {
  if (errors.length) {
    throw new BadRequest(errors.join(' '));
  }
};
