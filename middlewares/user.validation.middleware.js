const { user } = require('../models/user');
const { userValidator } = require('../helpers/userValidator');
const {
  deleteExtraFields,
  checkRequiredFields,
  validateFields,
  checkErrors,
} = require('./helpers');

const createUserValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    deleteExtraFields(fields, user, errors);
    checkRequiredFields(fields, user, errors);
    validateFields(fields, userValidator, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    deleteExtraFields(fields, user, errors);
    validateFields(fields, userValidator, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
