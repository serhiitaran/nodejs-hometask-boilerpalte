const { UserRepository } = require('../repositories/userRepository');
const { BadRequest, NotFound } = require('../helpers/customErrors');

class UserService {
  getUsers() {
    const users = UserRepository.getAll();
    if (!users) {
      throw new NotFound('Users not found');
    }
    return users;
  }

  getUser(id) {
    const user = this.search({ id });
    if (!user) {
      throw new NotFound('User not found');
    }
    return user;
  }

  createUser(userData) {
    const { email, phoneNumber } = userData;
    this.checkEmail(email);
    this.checkPhone(phoneNumber);
    const createdUser = UserRepository.create(userData);
    return createdUser;
  }

  updateUser(id, userData) {
    const userToUpdate = this.search({ id });
    if (!userToUpdate) {
      throw new NotFound(
        'User cannot be updated, because user not found',
      );
    }
    const { email, phoneNumber } = userData;
    this.checkEmail(email, id);
    this.checkPhone(phoneNumber, id);
    const updatedUser = UserRepository.update(id, userData);
    return updatedUser;
  }

  deleteUser(id) {
    const userToDelete = this.search({ id });
    if (!userToDelete) {
      throw new NotFound(
        'User cannot be deleted, because user not found',
      );
    }
    const deletedUser = UserRepository.delete(id);
    return deletedUser;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  checkEmail(email, id = null) {
    const user = this.search({ email });
    this.checkRegistered(user, 'email', id);
  }

  checkPhone(phoneNumber, id = null) {
    const user = this.search({ phoneNumber });
    this.checkRegistered(user, 'phoneNumber', id);
  }

  checkRegistered(user, field, id) {
    if (!user) {
      return;
    }
    const isAnotherId = !id ? true : user.id !== id;
    if (isAnotherId) {
      throw new BadRequest(
        `User with this ${field} is already registered`,
      );
    }
  }
}

module.exports = new UserService();
