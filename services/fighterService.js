const {
  FighterRepository,
} = require('../repositories/fighterRepository');
const { BadRequest, NotFound } = require('../helpers/customErrors');

class FighterService {
  getFighters() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      throw new NotFound('Fighters not found');
    }
    return fighters;
  }

  getFighter(id) {
    const fighter = this.search({ id });
    if (!fighter) {
      throw new NotFound('Fighter not found');
    }
    return fighter;
  }

  createFighter(data) {
    this.checkName(data.name);
    const createdFighter = FighterRepository.create(data);
    return createdFighter;
  }

  updateFighter(id, data) {
    const fighterToUpdate = this.search({ id });
    if (!fighterToUpdate) {
      throw new NotFound(
        'Fighter cannot be updated, because fighter not found',
      );
    }
    this.checkName(data.name, id);
    const updatedFighter = FighterRepository.update(id, data);
    return updatedFighter;
  }

  deleteFighter(id) {
    const fighterToDelete = this.search({ id });
    if (!fighterToDelete) {
      throw new NotFound(
        'Fighter cannot be deleted, because fighter not found',
      );
    }
    const deletedFighter = FighterRepository.delete(id);
    return deletedFighter;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  checkName(name, id = null) {
    const fighter = this.search({ name });
    if (!fighter) {
      return;
    }
    const isAnotherId = !id ? true : fighter.id !== id;
    if (isAnotherId) {
      throw new BadRequest(
        'Fighter with this name has already been created.',
      );
    }
  }
}

module.exports = new FighterService();
